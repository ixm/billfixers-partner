<?php

namespace BillfixersPartner;

/**
 * Build Saving Estimate query.
 */
interface SavingEstimateInterface {

  /**
   * Saving Estimate query.
   *
   * @param string $providerId
   *   The ID of the provider that is billing the customer.
   * @param float $current_monthly_payment
   *   The amount that the customer is currently paying the provider each month.
   *
   * @return \GraphQL\Query
   *   The GraphQL Query.
   */
  public function calculate(string $providerId, float $current_monthly_payment);

}

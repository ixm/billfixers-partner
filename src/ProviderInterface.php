<?php

namespace BillfixersPartner;

/**
 * Build query for Provider.
 */
interface ProviderInterface {

  /**
   * Provider list query.
   *
   * @return \GraphQL\Query
   *   The GraphQL Query.
   */
  public function list();

}

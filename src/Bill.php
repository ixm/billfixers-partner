<?php

namespace BillfixersPartner;

use GraphQL\Mutation;
use GraphQL\Query;
use GraphQL\RawObject;
use GraphQL\Variable;

/**
 * Build queries for Bill.
 */
class Bill implements BillInterface {

  const SELECTION_SET =
    [
      'id',
      'status',
      'title',
      'providerId',
      'totalSavings',
      'createdAt',
      'updatedAt',
      'autoRenegotiate',
      'allowsContract',
      'documentlessInfo',
      'missingDocumentInfo',
    ];

  /**
   * Creates a selection array for queries.
   *
   * @return static[]
   *   Array with selection values.
   */
  public static function fields() {
    return self::SELECTION_SET;
  }

  /**
   * {@inheritdoc}
   */
  public function list(int $limit = 25, int $offset = 0, string $customer_id = '') {
    $params = [
      'limit' => $limit,
      'offset' => $offset,
      'customerId' => $customer_id,
    ];

    $selection = [
      'totalCount',
      (new Query('nodes'))
        ->setSelectionSet(
          array_merge(
            Bill::fields(),
            [
              (new Query('items'))
                ->setSelectionSet(Item::fields()),
              (new Query('customer'))
                ->setSelectionSet(Customer::fields()),
              (new Query('provider'))
                ->setSelectionSet(
                  [
                    'id',
                    'name',
                    'services',
                    'billFields',
                    (new Query('logo'))
                      ->setSelectionSet(
                        [
                          'thumbnailUrl',
                          'smallUrl',
                          'mediumUrl',
                        ]
                    ),
                  ]
              ),
            ]
          )
      ),
    ];

    return (new Query('ListBills'))
      ->setArguments($params)
      ->setSelectionSet($selection);
  }

  /**
   * {@inheritdoc}
   */
  public function find(string $id) {
    $selection = [
      'id',
      'status',
      'title',
      'providerId',
      'totalSavings',
      'createdAt',
      'updatedAt',
      'missingDocumentInfo',
      'documentlessInfo',
      (new Query('items'))
        ->setSelectionSet(Item::fields()),
      (new Query('customer'))
        ->setSelectionSet(Customer::fields()),
      (new Query('offers'))
        ->setSelectionSet(Offer::fields()),
      (new Query('provider'))
        ->setSelectionSet(
          [
            'id',
            'name',
            'services',
            'billFields',
            (new Query('logo'))
              ->setSelectionSet(
                [
                  'thumbnailUrl',
                  'smallUrl',
                  'mediumUrl',
                ]
            ),
          ]
      ),
      (new Query('informationRequests'))
        ->setSelectionSet(array_merge(
            InformationRequest::fields(),
            [
              (new Query('fields'))
                ->setSelectionSet(InformationRequest::FIELDS_LIST),
            ]
          )
      ),
    ];

    return (new Query('FindBill'))
      ->setArguments(['id' => $id])
      ->setSelectionSet($selection);
  }

  /**
   * {@inheritdoc}
   */
  public function stopNegotiation() {
    $selection = [
      'id',
      'status',
      'title',
      'providerId',
      'totalSavings',
      'createdAt',
      'updatedAt',
      'autoRenegotiate',
      'allowsContract',
      'documentlessInfo',
      (new Query('items'))
        ->setSelectionSet(Item::fields()),
      (new Query('customer'))
        ->setSelectionSet(Customer::fields()),
      (new Query('provider'))
        ->setSelectionSet(Provider::fields()),
    ];

    return (new Mutation('StopNegotiating'))
      ->setVariables(
        [
          new Variable('id', 'ID', TRUE),
        ]
      )
      ->setArguments(
        [
          'input' => (new RawObject('{id: $id}')),
        ]
      )
      ->setSelectionSet(
        [
          'success',
          'errors',
          (new Query('bill'))
            ->setSelectionSet($selection),
        ]
      );
  }

  /**
   * {@inheritdoc}
   */
  public function create() {
    return (new Mutation('CreateBill'))
      ->setVariables(
        [
          new Variable('customer_id', 'ID', TRUE),
          new Variable('provider_id', 'ID', TRUE),
          new Variable('bill', 'BillAttributes', TRUE),
        ]
      )
      ->setArguments(
        [
          'input' => (new RawObject('{customerId: $customer_id, providerId: $provider_id, bill: $bill}')),
        ]
      )
      ->setSelectionSet([
        'success',
        'errors',
        (new Query('bill'))
          ->setSelectionSet(
            array_merge(
              Bill::fields(),
              [
                (new Query('items'))
                  ->setSelectionSet(Item::fields()),
                (new Query('customer'))
                  ->setSelectionSet(Customer::fields()),
                (new Query('provider'))
                  ->setSelectionSet(
                    [
                      'id',
                      'name',
                      'services',
                      'billFields',
                      (new Query('logo'))
                        ->setSelectionSet(
                          [
                            'thumbnailUrl',
                            'smallUrl',
                            'mediumUrl',
                          ]
                      ),
                    ]
                ),
              ]
            )
        ),
      ]);
  }

  /**
   * {@inheritdoc}
   */
  public function renegotiate() {
    $selection = [
      (new Query('items'))
        ->setSelectionSet(Item::fields()),
      (new Query('customer'))
        ->setSelectionSet(Customer::fields()),
      (new Query('provider'))
        ->setSelectionSet(Provider::fields()),
    ];

    return (new Mutation('RenegotiateBill'))
      ->setVariables(
        [
          new Variable('id', 'ID', TRUE),
        ]
      )
      ->setArguments(
        [
          'input' => (new RawObject('{id: $id}')),
        ]
      )
      ->setSelectionSet(
        [
          'success',
          'errors',
          (new Query('bill'))
            ->setSelectionSet(array_merge(Bill::fields(), $selection)),
        ]
      );
  }

  /**
   * {@inheritdoc}
   */
  public function provideDocumentlessInfo() {
    return (new Mutation('ProvideDocumentlessInfo'))
      ->setVariables(
        [
          new Variable('bill_id', 'ID', TRUE),
          new Variable('document_info', 'JSON', TRUE),
        ]
      )
      ->setArguments(
        [
          'input' => (new RawObject('{billId: $bill_id, documentlessInfo: $document_info}')),
        ]
      )
      ->setSelectionSet([
        'success',
        'errors',
        (new Query('bill'))
          ->setSelectionSet(
            array_merge(
              Bill::fields(),
              [
                (new Query('items'))
                  ->setSelectionSet(Item::fields()),
                (new Query('customer'))
                  ->setSelectionSet(Customer::fields()),
                (new Query('provider'))
                  ->setSelectionSet(
                    [
                      'id',
                      'name',
                      'services',
                      'billFields',
                      (new Query('logo'))
                        ->setSelectionSet(
                          [
                            'thumbnailUrl',
                            'smallUrl',
                            'mediumUrl',
                          ]
                      ),
                    ]
                ),
              ]
            )
        ),
      ]);
  }

  /**
   * {@inheritdoc}
   */
  public function update() {
    $selection = [
      (new Query('items'))
        ->setSelectionSet(Item::fields()),
      (new Query('customer'))
        ->setSelectionSet(Customer::fields()),
      (new Query('provider'))
        ->setSelectionSet(
          [
            'id',
            'name',
            'services',
            'billFields',
            (new Query('logo'))
              ->setSelectionSet(
                [
                  'thumbnailUrl',
                  'smallUrl',
                  'mediumUrl',
                ]
            ),
          ]
      ),
    ];
    return (new Mutation('UpdateBill'))
      ->setVariables(
        [
          new Variable('bill_id', 'ID', TRUE),
          new Variable('bill', 'BillAttributes', TRUE),
        ]
      )
      ->setArguments(
        [
          'input' => (new RawObject('{billId: $bill_id, bill: $bill}')),
        ]
      )
      ->setSelectionSet([
        'success',
        'errors',
        (new Query('bill'))
          ->setSelectionSet(
            array_merge(Bill::fields(), $selection)
        ),
      ]);
  }

}

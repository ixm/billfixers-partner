<?php

namespace BillfixersPartner;

/**
 * Build selection set for Item.
 */
class Item {

  const SELECTION_SET =
    [
      'name',
      'prePrice',
      'postPrice',
      'duration',
      'savings',
      'savingsStartOn',
      'savingsEndOn',
      'underContract',
      'createdAt',
      'updatedAt',
    ];

  /**
   * Creates a selection array for queries.
   *
   * @return static[]
   *   Array with selection values.
   */
  public static function fields() {
    return self::SELECTION_SET;
  }

}

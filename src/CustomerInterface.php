<?php

namespace BillfixersPartner;

/**
 * Build queries for Customer.
 */
interface CustomerInterface {

  /**
   * Customer list query.
   *
   * @param int $limit
   *   (optional) The number of customers you'd like returned by this request.
   *   Defaults to 25.
   * @param int $offset
   *   (optional) The number of customers to skip..
   *   Defaults to 0.
   *
   * @return \GraphQL\Query
   *   The total number of customers that matched
   *   the given parameters and an array of those customers.
   */
  public function list(int $limit = 25, int $offset = 0);

  /**
   * Create a customer query.
   *
   * @return \GraphQL\Query
   *   The customer object that was created if successful.
   *    If the request failed, an array of errors will be returned
   *    and the success field will be false.
   */
  public function create();

  /**
   * Find a customer query.
   *
   * @param string $id
   *   The ID of the customer.
   *
   * @return \GraphQL\Query
   *   The customer object, if found.
   *   If the request failed, an array of errors will be returned
   *   and the success field will be false.
   */
  public function find(string $id);

}

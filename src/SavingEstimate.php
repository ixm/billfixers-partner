<?php

namespace BillfixersPartner;

use GraphQL\Query;

/**
 * Build Saving Estimate query.
 */
class SavingEstimate implements SavingEstimateInterface {

  /**
   * {@inheritdoc}
   */
  public function calculate(string $providerId, float $current_monthly_payment) {
    $arguments = [
      'providerId' => $providerId,
      'currentMonthlyAmount' => $current_monthly_payment,
    ];

    return (new Query('CalculateSavingsEstimate'))
      ->setSelectionSet(
        [
          (new Query('estimatedAnnualSavings'))
            ->setArguments($arguments),
          (new Query('estimatedMonthlySavings'))
            ->setArguments($arguments),
          (new Query('percentageSavings'))
            ->setArguments($arguments),
        ]
      );
  }

}

<?php

namespace BillfixersPartner;

use GraphQL\Query;

/**
 * Build query for Provider.
 */
class Provider implements ProviderInterface {

  const SELECTION_SET =
    [
      'id',
      'name',
      'services',
      'billFields',
    ];

  /**
   * Creates a selection array for queries.
   *
   * @return static[]
   *   Array with selection values.
   */
  public static function fields() {
    return self::SELECTION_SET;
  }

  /**
   * {@inheritdoc}
   */
  public function list() {
    return (new Query('ListProviders'))
      ->setArguments([])
      ->setSelectionSet(
        [
          'id',
          'name',
          'services',
          'billFields',
          (new Query('logo'))
            ->setSelectionSet(
              [
                'thumbnailUrl',
                'smallUrl',
                'mediumUrl',
              ]
          ),
          (new Query('providerBillFields'))
            ->setSelectionSet(
              [
                'id',
                'providerId',
                'label',
                'name',
                'placeholder',
                'dataType',
                'required',
                'requiredResponseLength',
                'helpText',
                'formatHashString',
                'createdAt',
                'updatedAt',
              ]
          ),
        ]
      );
  }

}

<?php

namespace BillfixersPartner;

use GraphQL\Mutation;
use GraphQL\Query;
use GraphQL\RawObject;
use GraphQL\Variable;

/**
 * Build queries for Customer.
 */
class Customer implements CustomerInterface {

  const SELECTION_SET =
    [
      'id',
      'email',
      'name',
      'firstName',
      'lastName',
      'phoneNumber',
      'b2b',
      'createdAt',
      'updatedAt',
    ];

  /**
   * Creates a selection array for queries.
   *
   * @return static[]
   *   Array with selection values.
   */
  public static function fields() {
    return self::SELECTION_SET;
  }

  /**
   * {@inheritdoc}
   */
  public function list(int $limit = 25, int $offset = 0) {
    return (new Query('ListCustomers'))
      ->setArguments(
        [
          'limit' => $limit,
          'offset' => $offset,
        ]
      )
      ->setSelectionSet(
        [
          'totalCount',
          (new Query('nodes'))
            ->setSelectionSet(
            Customer::fields()),
        ]
      );
  }

  /**
   * {@inheritdoc}
   */
  public function create() {
    return (new Mutation('CreateCustomer'))
      ->setVariables(
        [
          new Variable('customer', 'CustomerAttributes', TRUE),
        ]
      )
      ->setArguments(
        [
          'input' => (new RawObject('{ customer: $customer }')),
        ]
      )
      ->setSelectionSet(
        [
          'success',
          'errors',
          (new Query('customer'))
            ->setSelectionSet(
              Customer::fields()),
        ]
      );
  }

  /**
   * {@inheritdoc}
   */
  public function find(string $id) {
    return (new Query('FindCustomer'))
      ->setArguments(['id' => $id])
      ->setSelectionSet(
        Customer::fields());
  }

}

<?php

namespace BillfixersPartner;

/**
 * Build queries for Information Request.
 */
interface InformationRequestInterface {

  /**
   * Respond to an information request.
   *
   * @return \GraphQL\Query
   *   The information request object that was responded to, if successful.
   *   If the request failed, an array of errors
   *   will be returned and the success field will be false.
   */
  public function respond();

  /**
   * Find an information request query.
   *
   * @param string $id
   *   The ID of the information request.
   *
   * @return \GraphQL\Query
   *   The information request object, if found.
   */
  public function find(string $id);

  /**
   * List information request query.
   *
   * @param int $limit
   *   (optional) The number of info requests
   *   you'd like returned by this request.
   *   Defaults to 25.
   * @param int $offset
   *   (optional) The number of info requests to skip.
   *   Defaults to 0.
   * @param string $customer_id
   *   (optional) If provided, the response will only include info requests
   *   owned by the customer associated with the given ID.
   *   Defaults to empty string.
   *
   * @return \GraphQL\Query
   *   The total numbers of info requests that matched
   *   the given parameters and an array of those info requests.
   */
  public function list(int $limit = 25, int $offset = 0, string $customer_id = '');

}

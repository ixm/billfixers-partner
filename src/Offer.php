<?php

namespace BillfixersPartner;

use GraphQL\Mutation;
use GraphQL\Query;
use GraphQL\RawObject;
use GraphQL\Variable;

/**
 * Build queries for Offer.
 */
class Offer implements OfferInterface {

  const SELECTION_SET =
    [
      'id',
      'status',
      'content',
      'contentHtml',
      'daysUntilExpiration',
      'createdAt',
      'updatedAt',
      'acceptedAt',
      'rejectedAt',
    ];

  /**
   * Creates a selection array for queries.
   *
   * @return static[]
   *   Array with selection values.
   */
  public static function fields() {
    return self::SELECTION_SET;
  }

  /**
   * {@inheritdoc}
   */
  public function find(string $id) {
    $selection = $this->getSelection();

    return (new Query('FindOffer'))
      ->setArguments(['offerId' => $id])
      ->setSelectionSet($selection);
  }

  /**
   * {@inheritdoc}
   */
  public function list(int $limit = 25, int $offset = 0, string $customer_id = '', string $bill_id = '', string $status = '') {
    $selection = $this->getSelection();

    return (new Query('ListOffers'))
      ->setArguments(
        [
          'limit' => $limit,
          'offset' => $offset,
          'customerId' => $customer_id,
          'billId' => $bill_id,
          'status' => $status,
        ]
      )
      ->setSelectionSet(
        [
          'totalCount',
          (new Query('nodes'))
            ->setSelectionSet($selection),
        ]
      );
  }

  /**
   * {@inheritdoc}
   */
  public function acceptOffer() {
    $selection = $this->getSelection();
    $mutation = (new Mutation('AcceptOffer'))
      ->setVariables([new Variable('id', 'ID', TRUE)])
      ->setArguments(
        [
          'input' => (new RawObject('{offerId: $id}')),
        ]
      )
      ->setSelectionSet(
        [
          'success',
          'errors',
          (new Query('offer'))
            ->setSelectionSet($selection),
        ]
      );

    return $mutation;
  }

  /**
   * {@inheritdoc}
   */
  public function rejectOffer() {
    $selection = $this->getSelection();

    $mutation = (new Mutation('RejectOffer'))
      ->setVariables([new Variable('id', 'ID', TRUE)])
      ->setArguments(
        [
          'input' => (new RawObject('{offerId: $id}')),
        ]
      )
      ->setSelectionSet(
        [
          'success',
          'errors',
          (new Query('offer'))
            ->setSelectionSet($selection),
        ]
      );

    return $mutation;
  }

  /**
   * Return selection set array.
   *
   * @return array
   *   The array of fields for query.
   */
  protected function getSelection() {
    $selection = [
      (new Query('customer'))
        ->setSelectionSet(Customer::fields()),
      (new Query('bill'))
        ->setSelectionSet(
          [
            'id',
            'status',
            'title',
            'providerId',
            'totalSavings',
            'createdAt',
            'updatedAt',
            'autoRenegotiate',
            'allowsContract',
            'documentlessInfo',
            (new Query('items'))
              ->setSelectionSet(Item::fields()),
            (new Query('customer'))
              ->setSelectionSet(Customer::fields()),
            (new Query('provider'))
              ->setSelectionSet(Provider::fields()),
          ]
      ),
    ];

    return array_merge(Offer::fields(), $selection);
  }

}

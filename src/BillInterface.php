<?php

namespace BillfixersPartner;

/**
 * Build queries for Bill.
 */
interface BillInterface {

  /**
   * List bills query.
   *
   * @param int $limit
   *   (optional) The number of bills you'd like returned by this request.
   *   Defaults to 25.
   * @param int $offset
   *   (optional) The number of bills to skip.
   *   Defaults to 0.
   * @param string $customer_id
   *   (optional) If provided, the response will only include bills owned
   *   by the customer associated with the given ID.
   *   Defaults to empty string.
   *
   * @return \GraphQL\Query
   *   The total number of customers that matched
   *   the given parameters and an array of those customers.
   */
  public function list(int $limit = 25, int $offset = 0, string $customer_id = '');

  /**
   * Find a bill query.
   *
   * @param string $id
   *   The ID of the bill.
   *
   * @return \GraphQL\Query
   *   The bill object, if found.
   */
  public function find(string $id);

  /**
   * Stop negotiating.
   *
   * If a bill is currently being negotiated, negotiations cannot be stopped.
   * Negotiations can be stopped before negotiation begins,
   * Or after they've finished, but not during the process.
   *
   * @return \GraphQL\Query
   *   An object containing a boolean success field indicating
   *   if negotiations were successfully stopped or not,
   *   an errors array containing errors (if any), and the bill object.
   *   Please note, there are several cases when the bill object will be null.
   *   If we have not yet attempted negotiation or the bill has been negotiated
   *   but no savings were found,
   *   calling stop_negotiating will effectively delete the bill,
   *   and the returned bill object will be null.
   */
  public function stopNegotiation();

  /**
   * Create a bill query.
   *
   * @return \GraphQL\Query
   *   The bill object that was created if successful.
   *   If the request failed, an array of errors
   *   will be returned and the success field will be false.
   */
  public function create();

  /**
   * Renegotiate a bill.
   *
   * @return \GraphQL\Query
   *   The bill object, if found.
   */
  public function renegotiate();

  /**
   * Provide documents info.
   *
   * @return \GraphQL\Query
   *   An object containing a boolean success field indicating
   *   if the documentless info was successfully recorded,
   *   an errors array containing errors (if any), and the bill object.
   */
  public function provideDocumentlessInfo();

  /**
   * Update a bill query.
   *
   * @return \GraphQL\Query
   *   The bill object that was updated if successful.
   *   If the request failed, an array of errors
   *   will be returned and the success field will be false.
   */
  public function update();

}

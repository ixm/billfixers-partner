<?php

namespace BillfixersPartner;

/**
 * Build queries for Offer.
 */
interface OfferInterface {

  /**
   * Find an offer query.
   *
   * @param string $id
   *   The ID of the customer.
   *
   * @return \GraphQL\Query
   *   The offer object, if found.
   */
  public function find(string $id);

  /**
   * List offers query.
   *
   * @param int $limit
   *   (optional) The number of offers you'd like returned by this request.
   *   Defaults to 25.
   * @param int $offset
   *   (optional) The number of offers to skip.
   *   Defaults to 0.
   * @param string $customer_id
   *   (optional) If provided, the response will only include offers
   *   associated with the given customer ID.
   *   Defaults to empty string.
   * @param string $bill_id
   *   (optional) If provided, the response will only include offers
   *   associated with the given bill ID.
   *   Defaults to empty string.
   * @param string $status
   *   Accepted values: pending, accepted, rejected.
   *   (optional) If provided, the response will only include offers
   *   with the given status.
   *   Defaults to empty string.
   *
   * @return \GraphQL\Query
   *   The total number of customers that matched
   *   the given parameters and an array of those customers.
   */
  public function list(int $limit = 25, int $offset = 0, string $customer_id = '', string $bill_id = '', string $status = '');

  /**
   * Accept an offer.
   *
   * @return \GraphQL\Query
   *   The offer object that was accepted if successful.
   *   If the request failed, an array of errors will be returned
   *   and the success field will be false.
   */
  public function acceptOffer();

  /**
   * Reject an offer.
   *
   * @return \GraphQL\Query
   *   The offer object that was rejected if successful.
   *   If the request failed, an array of errors will be returned
   *   and the success field will be false.
   */
  public function rejectOffer();

}

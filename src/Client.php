<?php

namespace BillfixersPartner;

use GraphQL\Client as BillfixersClient;
use GraphQL\Query;

/**
 * Billfixer client.
 */
class Client implements ClientInterface {

  /**
   * The Client object.
   *
   * @var \GraphQL\Client
   */
  protected $client;

  /**
   * Client constructs.
   *
   * @param string $apiKey
   *   X Partner Api Key.
   * @param string $apiUrl
   *   Where will we send query, to local url or prod.
   */
  public function __construct(string $apiKey, string $apiUrl) {
    $this->client = new BillfixersClient($apiUrl,
      ['X-Partner-ApiKey' => $apiKey]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function runQuery(Query $query, bool $resultsAsArray = FALSE, array $variables = []) {
    return $this->client->runQuery($query, $resultsAsArray, $variables);
  }

}

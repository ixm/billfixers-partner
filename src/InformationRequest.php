<?php

namespace BillfixersPartner;

use GraphQL\Mutation;
use GraphQL\Query;
use GraphQL\RawObject;
use GraphQL\Variable;

/**
 * Build queries for Information Request.
 */
class InformationRequest implements InformationRequestInterface {

  const SELECTION_SET =
    [
      'id',
      'content',
      'contentHtml',
      'createdAt',
      'respondedAt',
    ];

  const FIELDS_LIST =
    [
      'id',
      'label',
      'placeholder',
      'dataType',
      'value',
    ];

  /**
   * Creates a selection array for queries.
   *
   * @return static[]
   *   Array with selection values.
   */
  public static function fields() {
    return self::SELECTION_SET;
  }

  /**
   * {@inheritdoc}
   */
  public function respond() {
    $selection = [
      (new Query('fields'))
        ->setSelectionSet(InformationRequest::FIELDS_LIST),
    ];

    $mutation = (new Mutation('RespondToInformationRequest'))
      ->setVariables(
        [
          new Variable('id', 'ID', TRUE),
          new Variable('ir', 'InformationRequestAttributes', TRUE),
        ]
      )
      ->setArguments(
        [
          'input' => (new RawObject('{id: $id, informationRequest: $ir}')),
        ]
      )
      ->setSelectionSet(
        [
          'success',
          'errors',
          (new Query('informationRequest'))
            ->setSelectionSet($selection),
        ]
      );

    return $mutation;
  }

  /**
   * {@inheritdoc}
   */
  public function find(string $id) {
    $customer_bill_fields = [
      (new Query('customer'))
        ->setSelectionSet(
          [
            'id',
            'email',
          ]
      ),
      (new Query('bill'))
        ->setSelectionSet(
          [
            'id',
          ]
      ),
    ];
    $selection = [
      (new Query('fields'))
        ->setSelectionSet(InformationRequest::FIELDS_LIST),
    ];
    $fields_list = array_merge($selection, $customer_bill_fields);

    return (new Query('FindInformationRequest'))
      ->setArguments(['informationRequestId' => $id])
      ->setSelectionSet(array_merge(InformationRequest::fields(), $fields_list));
  }

  /**
   * {@inheritdoc}
   */
  public function list(int $limit = 25, int $offset = 0, string $customer_id = '') {
    $params = [
      'limit' => $limit,
      'offset' => $offset,
      'customerId' => $customer_id,
    ];
    $selection = [
      'totalCount',
      (new Query('nodes'))
        ->setSelectionSet(
          [
            'id',
            'content',
            'contentHtml',
            'createdAt',
            'respondedAt',
            (new Query('fields'))
              ->setSelectionSet(InformationRequest::FIELDS_LIST),
            (new Query('bill'))
              ->setSelectionSet(
                [
                  'id',
                ]
            ),
          ]),
    ];

    return (new Query('ListInformationRequests'))
      ->setArguments($params)
      ->setSelectionSet($selection);
  }

}

<?php

namespace BillfixersPartner;

use GraphQL\Query;

/**
 * Billfixer client.
 */
interface ClientInterface {

  /**
   * Run query method.
   *
   * @param \GraphQL\Query|\GraphQL\QueryBuilder\QueryBuilderInterface $query
   *   Query for run.
   * @param bool $resultsAsArray
   *   Return result as array.
   * @param array $variables
   *   Array with variables for query.
   */
  public function runQuery(Query $query, bool $resultsAsArray = FALSE, array $variables = []);

}

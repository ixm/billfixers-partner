# Billfixers library
[Version](https://bitbucket.org/ixm/billfixers-partner/src)
[[Latest Stable Version](https://bitbucket.org/ixm/billfixers-partner/src/master/)]

# Usage
Billfixers library allows you to query Billfixers API using GraphQL client.

1. Reach Billfixers team so as to have a partner account https://billfixers.com/partnerships 
2. Use the Partner ApiKey to instantiate a Client class.
3. Needs to create your own client to authenticate with Billfixers and run any queries, using Client class.
4. Create your own custom implementation to use queries from this library.

# Examples

## Create Customer
```
<?php

use BillfixersPartner\Customer;
use BillfixersPartner\Client;

/**
 * Create Customer example.
 */
function fooBar() {    
    // Instantiate the Billfixers client.
    $client = new Client($api_key, $api_url);

    // Instantiate the Provider class and get the list query.
    $customer = new Customer();
    $variables = [
      'email' => $email,
      'firstName' => $first_name,
      'lastName' => $last_name,
      'phoneNumber' => $phone_number,
    ];

    $variables = ['customer' => $variables];    
    
    // Execute the query.
    // Parameters available: Query $query, bool $resultsAsArray = FALSE, array $variables = []
    $result = $client->runQuery($query, TRUE, $variables);
}
```

## List providers
```
<?php

use BillfixersPartner\Provider;
use BillfixersPartner\Client;

/**
 * List Providers example.
 */
function fooBar() {    
    // Instantiate the Billfixers client.
    $client = new Client($api_key, $api_url);

    // Instantiate the Provider class and get the list query.
    $provider = new Provider();
    $query = $provider->list();
    
    // Execute the query.
    // Parameters available: Query $query, bool $resultsAsArray = FALSE, array $variables = []
    $result = $client->runQuery($query);
}
```